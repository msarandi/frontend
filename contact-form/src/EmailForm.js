
import React, { useState } from 'react';
import './App.css';


function EmailForm() {
  const [firstName, setfirstName] = useState();
  const [lastName, setlastName] = useState();
  const [email, setEmail] = useState();


/* POST to JSON file */
  function handleSubmit(event) {
    event.preventDefault();
    console.log(firstName, lastName, email);
    let url = "http://localhost:8001/contact.html"
    fetch(url, {
      method: 'POST',
      body: JSON.stringify({ firstName, lastName, email }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then(response => response.json())
      .then(response => console.log(response))
      .catch(error => console.log(error))
  }

  function handlefirstNameInput(event) {
    setfirstName(event.target.value)
  }

  function handlelastNameInput(event) {
    setlastName(event.target.value)
  }

  function handleEmailInput(event) {
    setEmail(event.target.value)
  }

  return (

    /* Email Submission Form */
    <form className="email-entry" onSubmit={handleSubmit}>

      <h5 className="h5 mb-3 font-weight-normal">Let's connect!</h5>

      <label htmlFor="inputfirstName" className="first">First Name  </label>
      <input onChange={handlefirstNameInput} type="first" id="firstName" className="form-control" placeholder="" required autoFocus />
      <br /><label htmlFor="inputlastName" className="last">Last Name  </label>
      <input onChange={handlelastNameInput} type="last" id="lastName" className="form-control" placeholder="" required />
      <br /><label htmlFor="inputemail " className="email">Email  </label>
      <input onChange={handleEmailInput} type="email" id="email" className="form-control" placeholder="example@email.com" required />
      <br />
      <button className="btn btn-sm btn-outline-dark btn-block" type="submit">Submit</button>

    </form>
  )
}



export default EmailForm;
